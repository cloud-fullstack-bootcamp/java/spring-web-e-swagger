package oak.web.demo.controller;

import oak.web.demo.model.Usuario;
import oak.web.demo.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/{users}")
public class UsuarioController {
    @Autowired
    private UsuarioRepository repository;

    @GetMapping("/")
    public List<Usuario>getUsers(){
        return repository.findAll();
    }

    @GetMapping("/{username}")
    public Usuario getUsuario(@PathVariable("username") String username){
        return repository.findByUsername(username);
    }

    @DeleteMapping("/{id}")
    public void deleteUsuario(@PathVariable("id") Integer id){
        repository.deleteById(id);
    }

    @PostMapping("")
    public void postUsuario(@RequestBody Usuario usuario){
        repository.save(usuario);
    }

    @PutMapping()
    public void putUser(@RequestBody Usuario usuario){
        repository.save(usuario);
    }
}
